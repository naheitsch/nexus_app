import 'package:flutter/material.dart';
import 'dart:async';
import 'package:nexus_app/services/AuthorizedClient.dart';
import 'package:nexus_app/enum/TaskStatus.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> with TickerProviderStateMixin{
  BuildContext _scaffoldContext;
  TextEditingController usernameTextController = new TextEditingController();
  TextEditingController passwordTextController = new TextEditingController();
  final formKey = new GlobalKey<FormState>();

  List<Color> colors ;

  bool _isPressed = false;
  bool _animatingReveal = false;
  int _state = 0;
  double _width = 248.0;
  Animation _animation;
  GlobalKey _globalKey = GlobalKey();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    colors = [Theme.of(context).primaryColor, Theme.of(context).primaryColor, Colors.green, Colors.red];
    Widget body =
new Container(
        padding: new EdgeInsets.all(16.0),
        child: new Center(
          child: new Card(
            child: new Padding(
              padding: const EdgeInsets.all(16.0),
              child: new Form(
                key: formKey,
                autovalidate: false,
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: new Text('Nexus', style: new TextStyle(fontSize: 24.0)),
                    ),
                    new Divider(),
                    new TextFormField(
                      controller: usernameTextController,
                      keyboardType: TextInputType.emailAddress,
                      autofocus: true,
                      autocorrect: false,
                      validator: (val) => val.length <= 3? 'Username can\'t be empty.' : null,
                      decoration: new InputDecoration(
                          labelText: 'Username',
                          icon: new Icon(Icons.account_circle),
                      )
                    ),
                    new TextFormField(
                      controller: passwordTextController,
                      keyboardType: TextInputType.text,
                      autocorrect: false,
                      validator: (val) => val.isEmpty? 'Password can\'t be empty.' : null,
                      obscureText: true,
                      decoration: new InputDecoration(
                          labelText: 'Password',
                          icon: new Icon(Icons.lock_outline)),
                    ),
                    new Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: new Container(
                        padding: new EdgeInsets.all(8.0),
                        key: _globalKey,
                        height: 64.0,
                        width: _width,
                        child: new RaisedButton(
                          shape: getCorners(),
                          padding: EdgeInsets.all(8.0),
                          color: colors[_state],
                          onPressed: () {},
                          child: buildButtonChild(),
                            onHighlightChanged: (isPressed) {
                              setState(() {
                                _isPressed = isPressed;
                                if (_state == 0) {
                                  animateButton();
                                }
                              });
                            }
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      );
    return new Scaffold(
      body: new Builder(
          builder: (BuildContext context) {
            _scaffoldContext = context;
            return body;
          }
      ),
    );

  }

  RoundedRectangleBorder getCorners() {
    if (_state != 0)
      return new RoundedRectangleBorder(
          borderRadius: new BorderRadius.all(new Radius.circular(24.0))
      );
    return null;
  }
  void animateButton() {
    final form = formKey.currentState;
    if (!form.validate()) {
      return;
    }
    double initialWidth = 248.0;

    var controller =
    AnimationController(duration: Duration(milliseconds: 300), vsync: this);

    _animation = Tween(begin: 0.0, end: 1.0).animate(controller)
      ..addListener(() {
        setState(() {
          _width = initialWidth - ((initialWidth - 64.0) * _animation.value);
        });
      });

    controller.forward();

    setState(() {
      _state = 1;
    });

    attemptAuthentication().then((a) async {
      if(a == TaskStatus.FAILURE){
        setState(() {
          _state = 3;
        });
        await new Future.delayed(const Duration(seconds : 3));

        controller.reverse();

        setState(() {
          _state = 0;
        });

      }else {
        setState(() {
          _state = 2;
        });
      }
    });
  }

  Widget buildButtonChild() {
    if (_state == 0) {
      return new Text('Sign In', style: new TextStyle(color: Colors.white, fontWeight: FontWeight.bold));
    } else if (_state == 1) {
      return SizedBox(
      height: 36.0,
      width: 36.0,
      child: CircularProgressIndicator(
      value: null,
      valueColor: AlwaysStoppedAnimation<Color>(Colors.white)));
    } else if(_state == 2) {
      return Icon(Icons.check, color: Colors.white);
    }
    return Icon(Icons.close, color: Colors.white);
  }

  Future<TaskStatus> attemptAuthentication() async {
    debugPrint(' - Attempting to Authenticate - ');
    debugPrint('\tUsername: ' + usernameTextController.text);
    debugPrint('\tPassword: ' + passwordTextController.text);
    return await AuthorizedClient.authenticate(username: usernameTextController.text,
        password: passwordTextController.text).then((value) {
      debugPrint('\tStatus: ' + value.toString());
      if (value == TaskStatus.SUCCESS) {
          Timer(new Duration(seconds: 2), () {
            Navigator.pop(this.context);
          });
      } else {
        Scaffold.of(_scaffoldContext).showSnackBar(new SnackBar(
          content: new Text("There was an issue signing in."),
        ));
        return TaskStatus.FAILURE;
      }
    }).catchError((error) {
      Scaffold.of(_scaffoldContext).showSnackBar(new SnackBar(
        content: new Text("There was an issue signing in."),
      ));
      return TaskStatus.FAILURE;
    });
  }
}
