import 'dart:async';

import 'package:flutter/material.dart';
import 'package:nexus_app/models/models.dart';
import 'package:nexus_app/redux/redux.dart';
import 'package:nexus_app/services/AuthorizedClient.dart';
import 'package:intl/intl.dart';
import 'package:nexus_app/ui/presenters/Attendance.dart';
class EventsPage extends StatefulWidget {
  @override
  _EventsPageState createState() => _EventsPageState();
}

class _EventsPageState extends State<EventsPage> with TickerProviderStateMixin {
  BuildContext _scaffoldContext;
  TabController tabController;

  ///
  /// Creates and returns the tabs for the tab bar
  List<Widget> _getTabs(Store<AppState> store){
    List<Widget> tabs = [];
    store.state.eventTypes.forEach((type) {
      tabs.add(new Tab(text: type.name,));
    });
    return tabs;
  }

  ///
  /// Returns pages for all the tabs
  List<Widget> _getPages(Store<AppState> store, context){
    List<Widget> pages = [];
    store.state.eventTypes.forEach((type) {
      List<Widget> list = [];
      store.state.events.sort((a,b) => b.date.compareTo(a.date));
      store.state.events.where((event) => event.event_type == type.id).forEach((event) {
        var formatter = new DateFormat('yMMMMEEEEd');
        String formatted = formatter.format(event.date);
        if(event.name == null) {
          list.add(new ListTile(
            title: new Text(formatted),
            onTap: () {
              Navigator.push(context, new MaterialPageRoute(
                  builder: (context) => new AttendancePage(
                      event.id, formatted, store.state.eventTypes
                      .firstWhere((type) => type.id == event.event_type)
                      .name)));
            },
          ));
        }else {
          list.add(new ListTile(
            title: new Text(formatted),
            subtitle: new Text(event.name),
            onTap: () {
              Navigator.push(context, new MaterialPageRoute(
                  builder: (context) => new AttendancePage(
                      event.id, formatted, store.state.eventTypes
                      .firstWhere((type) => type.id == event.event_type)
                      .name)));
            },
          ));
        }
      });
      pages.add(new RefreshIndicator(
        onRefresh: () {
          var action = new PullEventsAction();
          store.dispatch(action);
          return action.completer.future;
        },
        child: new ListView(
          children: list,
        ),
      ),
      );
    });
    return pages;
  }

  ///
  /// Returns a widget for the entire page
  Widget _getTabController(Store<AppState> store, context) {
    return new Scaffold(
      appBar: new AppBar(
        title: const Text('Events'),
        bottom: _getTabBar(store),
      ),
      body: new TabBarView(
        controller: tabController,
        children: _getPages(store, context),
      ),
      floatingActionButton: store.state.user.permissions.where(
              (permission) => permission.name.contains('All') || permission.name.contains('Records'))
              .length != 0 ? new FloatingActionButton(
          onPressed: () => _showAddEventDialog(store),
          child: new Icon(Icons.add)
      ): null,
    );

  }

  ///
  /// Gets creates and returns the tab bar widget
  Widget _getTabBar(Store<AppState> store) {
    if(store.state.eventTypes.length > 1) {
      return new TabBar(
        tabs: _getTabs(store),
        controller: tabController,
      );
    }else{
      return null;
    }
  }

  ///
  /// Shows a DatePicker and creates a new event if ok is selected
  void _showAddEventDialog(Store<AppState> store) {
    DateTime current = new DateTime.now();
    showDatePicker(
        context: context,
        initialDate: current,
        firstDate: new DateTime(current.year - 1),
        lastDate: new DateTime(current.year + 1)
    ).then((date) async {
      // Submit request for event
      var formatter = new DateFormat('yyyy-MM-dd');
      String formatted = formatter.format(date);
      AuthorizedClient.post(route: '/api/events', content: <String, String>{
        'event_type': store.state.eventTypes[tabController.index].id.toString(),
        'date': formatted,
        'term': store.state.terms[0].term.toString()
      })
          .then((value) {
        store.dispatch(AddEventAction(new Event.fromMap(value)));
      });
      Scaffold.of(_scaffoldContext).showSnackBar(new SnackBar(
        content: new Text("Event on " + formatted + " added."),
      ));
    })
    .catchError((error) {
      Scaffold.of(_scaffoldContext).showSnackBar(new SnackBar(
        content: new Text("Error adding event"),
      ));
      debugPrint(error.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, Store<AppState>>(
      converter: (store) => store,
      builder: (context, store) {
        if(store.state.events == null && store.state.isLoading['events']){
          return new Scaffold(
            body:  new Center(
            child: CircularProgressIndicator()
            )
          );
        }else if(store.state.events == null && !store.state.isLoading['events']){
          store.dispatch(PullEventsAction());
          if(store.state.terms == null) {
            print('Terms are null');
            store.dispatch(PullTermsAction());
          }
          return new Scaffold(
              body:  new Center(
                  child: CircularProgressIndicator()
              )
          );
        }else{
          print(store.state.terms);
          tabController  = new TabController(length: store.state.eventTypes.length, vsync: this);
          return new Scaffold(
            body: new Builder(
                builder: (BuildContext context) {
                  _scaffoldContext = context;
                  return _getTabController(store, context);
                }
            ),
          );
        }
      }
    );
  }
}
