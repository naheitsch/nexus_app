import 'dart:convert';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:nexus_app/models/models.dart';
import 'package:nexus_app/redux/redux.dart';
import 'package:nexus_app/ui/presenters/ProfileAvatar.dart';
import 'package:flutter/material.dart';
import 'package:nexus_app/services/AuthorizedClient.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:unicorndial/unicorndial.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/services.dart';
import 'Cadets.dart';
import 'dart:math';
import 'dart:async';

class AttendancePage extends StatefulWidget {
  int event_id;
  String date;
  String event_type;
  AttendancePage(this.event_id, this.date, this.event_type, {Key key})
      : super(key: key);

  @override
  _AttendancePageState createState() => new _AttendancePageState();
}

class _AttendancePageState extends State<AttendancePage>
    with TickerProviderStateMixin {
  BuildContext _scaffoldContext;
  TabController tabController;

  ///
  /// Returns pages for all the tabs
  List<Widget> _getAttendanceList(Store<AppState> store) {
    List<Widget> list = [];
    store.state.attendance
        .where((record) => record.event_id == widget.event_id)
        .forEach((record) {
      try {
        Cadet cadet = store.state.cadets
            .firstWhere((cadet) => cadet.id == record.cadet_id);
        list.add(new ListTile(
          leading: new ProfileAvatar(cadet.getInitials(), cadet.image_uri),
          title: new Text(cadet.getFullName()),
          subtitle: new Text(store.state.attendanceTypes
              .firstWhere((type) => type.id == record.attendance_type)
              .name),
        ));
      } catch (error) {
        print('User ' + record.cadet_id.toString() + ' is hidden.');
      }
    });
    return list;
  }

  _addRecordBatch(List<int> ids, Store<AppState> store) async {
    Map<String, String> att = new Map();
    att['event_id'] = widget.event_id.toString();
    ids.forEach((id) {
      att['attendance[' + id.toString() + ']'] = 4.toString();
    });

    await AuthorizedClient.post(
            route: '/api/attendance/batchupdate', content: att)
        .then((value) {
      print('batchatt');
      print(value);
      //Add Attendance to list and database
      for (var val in value) {
        Attendance attendance = new Attendance.fromMap(val);
        store.dispatch(AddAttendanceRecordAction(attendance));
      }
      Scaffold.of(_scaffoldContext).showSnackBar(new SnackBar(
        content: new Text("Attendance records added."),
      ));
    }).catchError((error) {
      print(error);
      Scaffold.of(_scaffoldContext).showSnackBar(new SnackBar(
        content: new Text("Error adding attendance"),
      ));
    });
  }

  _addRecord(int id, Store<AppState> store) async {
    await AuthorizedClient.post(
        route: '/api/attendance',
        content: <String, String>{
          'attendance_type': 4.toString(),
          'cadet_id': id.toString(),
          'event_id': widget.event_id.toString()
        }).then((value) {
      //Add Attendance to list and database
      Attendance attendance = new Attendance.fromMap(value);
      store.dispatch(AddAttendanceRecordAction(attendance));
      Scaffold.of(_scaffoldContext).showSnackBar(new SnackBar(
        content: new Text("Record for " +
            store.state.cadets
                .firstWhere((cadet) => cadet.id == attendance.cadet_id)
                .getFullName() +
            " added."),
      ));
    }).catchError((error) {
      Scaffold.of(_scaffoldContext).showSnackBar(new SnackBar(
        content: new Text("Error adding attendance"),
      ));
    });
  }

  ///
  /// Returns a widget for the entire page
  Widget _getContents(Store<AppState> store) {
    if (store.state.cadets == null || store.state.attendance == null) {
      if (store.state.cadets == null && !store.state.isLoading['cadets']) {
        store.dispatch(PullCadetsAction());
      }
      if (store.state.attendance == null &&
          !store.state.isLoading['attendance']) {
        store.dispatch(PullAttendanceAction());
      }
      return new Center(child: CircularProgressIndicator());
    }
    if (store.state.attendance
            .where((record) => record.event_id == widget.event_id)
            .length ==
        0) {
      return new Center(
        child: store.state.isLoading['attendance']
            ? new CircularProgressIndicator()
            : new GestureDetector(
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Icon(Icons.refresh, color: Colors.grey),
                    new Text('No Attendance Records',
                        style:
                            new TextStyle(fontSize: 16.0, color: Colors.grey)),
                    new Text('Tap to Reload',
                        style:
                            new TextStyle(fontSize: 12.0, color: Colors.grey)),
                  ],
                ),
                onTap: () async {
                  store.dispatch(PullAttendanceAction());
                },
              ),
      );
    }
    return new RefreshIndicator(
      onRefresh: () async {
        var action = new PullAttendanceAction();
        store.dispatch(action);
        return action.completer.future;
      },
      child: new ListView(
        children: _getAttendanceList(store),
      ),
    );
  }

  ///
  /// Shows a DatePicker and creates a new attendance if ok is selected
  void _showAddAttendanceDialog(Store<AppState> store) {
    DateTime current = new DateTime.now();
    showDatePicker(
            context: context,
            initialDate: current,
            firstDate: new DateTime(current.year - 1),
            lastDate: new DateTime(current.year + 1))
        .then((date) async {
      // Submit request for attendance
      var formatter = new DateFormat('yyyy-MM-dd');
      String formatted = formatter.format(date);

      AuthorizedClient.post(
          route: '/api/attendances',
          content: <String, String>{
            'attendance_type':
                store.state.attendanceTypes[tabController.index].id.toString(),
            'date': formatted,
            'term': store.state.terms[0].term.toString()
          }).then((value) {
        //Add Attendance to list and database
        Attendance attendance = new Attendance.fromMap(value);
        store.dispatch(AddAttendanceRecordAction(attendance));
        Scaffold.of(_scaffoldContext).showSnackBar(new SnackBar(
          content: new Text(store.state.attendanceTypes
                  .firstWhere((type) => type.id == attendance.attendance_type)
                  .name +
              " on " +
              formatted +
              " added."),
        ));
      }).catchError((error) {
        Scaffold.of(_scaffoldContext).showSnackBar(new SnackBar(
          content: new Text("Error adding attendance"),
        ));
      });
    }).catchError((error) {
      debugPrint(error.toString());
      Scaffold.of(_scaffoldContext).showSnackBar(new SnackBar(
        content: new Text("Error adding attendance"),
      ));
    });
  }

  @override
  void initState() {
    super.initState();
  }

  showScanner(BuildContext context, Store<AppState> store) async {
    List<CameraDescription> cameras = await availableCameras();

    CameraController controller;
    controller = new CameraController(cameras[0], ResolutionPreset.medium);
    controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
    });
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          if (!controller.value.isInitialized) {
            return new Container();
          }
          return new GestureDetector(
            child: new Stack(
              alignment: FractionalOffset.center,
              children: <Widget>[
                new Positioned.fill(
                  child: new AspectRatio(
                      aspectRatio: controller.value.aspectRatio,
                      child: new CameraPreview(controller)),
                ),
                new Positioned.fill(
                    child: new IgnorePointer(
                  child: new ClipPath(
                    clipper: new RectangleClipper(),
                    child: new Container(
                      color: new Color.fromRGBO(0, 0, 0, 0.5),
                    ),
                  ),
                )),
              ],
            ),
            /*AspectRatio(
            aspectRatio:
            controller.value.aspectRatio,
            child: new CameraPreview(controller)
          ),
          */
            onTap: () => scanID(controller, store),
          );
        });
  }

  String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();

  Future<String> takePicture(controller) async {
    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/pic_temp';
    await Directory(dirPath).create(recursive: true);
    final String filePath = '$dirPath/${timestamp()}.jpg';

    if (controller.value.isTakingPicture) {
      // A capture is already pending, do nothing.
      return null;
    }

    try {
      await controller.takePicture(filePath);
    } on CameraException catch (e) {
      print("Unable to save photo");
      return null;
    }
    return filePath;
  }

  Future<List<dynamic>> scanID(controller, Store<AppState> store) async {
    String ppath = await takePicture(controller);
    debugPrint(ppath);

    File file = File(ppath);
    final FirebaseVisionImage visionImage = FirebaseVisionImage.fromFile(file);

    BarcodeDetector detector = FirebaseVision.instance.barcodeDetector();

    final List<Barcode> results =
        await detector.detectInImage(visionImage) ?? <Barcode>[];
    print(results);
    if (results.length != 0) {
      print(results.first.displayValue);

      await AuthorizedClient.post(
          route: '/api/attendance/storebyuid',
          content: <String, String>{
            'attendance_type': 4.toString(),
            'rit_uid': results.first.displayValue.substring(0, 9),
            'event_id': widget.event_id.toString()
          }).then((value) {
        print(value);
        Attendance attendance = new Attendance.fromMap(value);
        store.dispatch(AddAttendanceRecordAction(attendance));
        Cadet temp = store.state.cadets
            .where((cadet) => cadet.id == value['cadet_id'])
            .first;
        /*
        Flushbar(
          message: "Attendance Record Updated",
          title: temp.getFullName(),
          flushbarPosition: FlushbarPosition.TOP, //Immutable
          isDismissible: true,
          duration: Duration(seconds: 4),
          showProgressIndicator: false,
        )..show(context);
        */
      }).catchError((error) {
        print(error);
        /*
        Flushbar(
          title: "There was an error.",
          message: "Something went wrong when sending the request.",
          flushbarPosition: FlushbarPosition.TOP, //Immutable
          isDismissible: true,
          duration: Duration(seconds: 4),
          backgroundColor: Colors.red,
          showProgressIndicator: false,
        )..show(context);
        */
      });
    } else {
      /*
      Flushbar(
        title: "No Barcode Found",
        message: "Try holding the card further from the device.",
        flushbarPosition: FlushbarPosition.TOP, //Immutable
        isDismissible: true,
        duration: Duration(seconds: 4),
        showProgressIndicator: false,
      )..show(context);
      */
    }

    return results;
  }

  @override
  Widget build(BuildContext context) {
    Color backgroundColor = Theme.of(context).cardColor;
    Color foregroundColor = Theme.of(context).accentColor;

    List<UnicornButton> childButtons = new List();
    return new StoreConnector<AppState, Store<AppState>>(
        converter: (store) => store,
        builder: (context, store) {
          return new Scaffold(
            appBar: new AppBar(
              title: const Text('Attendance'),
            ),
            body: new Builder(builder: (BuildContext context) {
              _scaffoldContext = context;
              return _getContents(store);
            }),
            floatingActionButton: store.state.user.permissions
                        .where((permission) =>
                            permission.name.contains('All') ||
                            permission.name.contains('Records') ||
                            permission.name.contains('EventScanning'))
                        .length !=
                    0
                ? UnicornDialer(
                    backgroundColor: Color.fromRGBO(255, 255, 255, 0.6),
                    parentButtonBackground: Theme.of(context).accentColor,
                    orientation: UnicornOrientation.VERTICAL,
                    parentButton: Icon(Icons.add, color: Colors.white),
                    childButtons: [
                        UnicornButton(
                            currentButton: FloatingActionButton(
                                heroTag: "Scan",
                                backgroundColor: Theme.of(context).accentColor,
                                mini: true,
                                onPressed: () => showScanner(context, store),
                                child:
                                    Icon(Icons.camera, color: Colors.white))),
                        UnicornButton(
                            currentButton: FloatingActionButton(
                                heroTag: "List",
                                backgroundColor: Theme.of(context).accentColor,
                                mini: true,
                                onPressed: () async {
                                  List<int> ids = await Navigator.push(
                                      context,
                                      new MaterialPageRoute(
                                          builder: (context) =>
                                              new CadetAddPage()));
                                  debugPrint(ids.toString());
                                  if (ids != null) {
                                    if (ids.length == 1) {
                                      for (var id in ids) {
                                        await _addRecord(id, store);
                                      }
                                    } else {
                                      await _addRecordBatch(ids, store);
                                    }
                                  }
                                },
                                child: Icon(
                                  Icons.view_list,
                                  color: Colors.white,
                                )))
                      ])
                : null,
          );
        });
  }
}

class RectangleClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    return new Path()
      ..addRect(new Rect.fromLTWH(0.0, 0.0, size.width, size.height))
      ..addRect(new Rect.fromLTWH(
          .1 * size.width, .3 * size.height, .8 * size.width, .4 * size.height))
      ..fillType = PathFillType.evenOdd;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
