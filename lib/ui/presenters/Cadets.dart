import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nexus_app/models/models.dart';
import 'package:nexus_app/redux/redux.dart';
import 'package:nexus_app/services/AuthorizedClient.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:material_search/material_search.dart';
import 'ProfileAvatar.dart';

class CadetsPage extends StatelessWidget {
  List<Widget> _getList(context, store){
    if(store.state.cadets == null) {
      return <Widget>[
        new CircularProgressIndicator(),
      ];
    }
    List<Widget> t = [];
    store.state.cadets.forEach((cadet) {
      t.add(new ListTile(
        leading: new ProfileAvatar(cadet.getInitials(), cadet.image_uri),
        title: new Text(cadet.firstName + ' ' + cadet.lastName),
        subtitle: new Text(cadet.position != null ? cadet.position.name : 'N/A'),
        onTap: () {
          Navigator.push(context, new MaterialPageRoute(builder: (context) => new ProfilePage(cadet)));
        },
      ));
    });
    return t;
  }
  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, Store<AppState>>(
      converter: (store) => store,
      builder: (context, store) {
        if(store.state.cadets == null && store.state.isLoading['cadets'] == false){
          store.dispatch(PullCadetsAction());
        }
        return new Scaffold(
          appBar: AppBar(
              title: const Text('Cadets')
          ),
          body: new RefreshIndicator(
            child: new ListView(children: _getList(context, store)),
            onRefresh: () async {
              final action = new PullCadetsAction();
              store.dispatch(action);
              return action.completer.future;
            },
          ),
          floatingActionButton: new FloatingActionButton(
              onPressed: () async {
                int id = await Navigator.push(context, new MaterialPageRoute(
                    builder: (context) => new CadetSearchPage()));
                if(id == null){
                  return;
                }
                Cadet cadet = store.state.cadets.where((cadet) => cadet.id == id).first;
                Navigator.push(context, new MaterialPageRoute(builder: (context) => new ProfilePage(cadet)));
              },
              child: new Icon(Icons.search)
          ),
        );
    });
  }
}



class ProfilePage extends StatelessWidget {
  Cadet cadet;

  ProfilePage(this.cadet);
  @override
  Widget build(BuildContext context) {
    TextStyle titleStyle = new TextStyle(fontSize: 12.0);
    TextStyle subTitleStyle = new TextStyle(fontSize: 16.0, color: new Color(0xFF111111));
    final key = new GlobalKey<ScaffoldState>();
    String image_uri;
    if(cadet.image_uri != null) {
      if (cadet.image_uri.contains('profile_placeholder')) {
        image_uri = cadet.image_uri;
      } else {
        image_uri = '/api' + cadet.image_uri;
      }
    }
    return new StoreConnector<AppState, Store<AppState>>(
      converter: (store) => store,
      builder: (context, store) {
        return new Scaffold(
          key: key,
          appBar: AppBar(
            title: new Text(cadet.firstName + ' ' + cadet.lastName),
          ),
          body: new ListView(
            children: <Widget>[
              new Card(
                child: new Container(
                  decoration: new BoxDecoration(
                    image: (cadet.image_uri != null) ? new DecorationImage(
                      image: AuthorizedClient.getImage(
                          image_uri, store.state.constants, store.state.accessToken),
                      fit: BoxFit.cover,) : null,
                  ),
                  alignment: Alignment.bottomCenter,
                  constraints: new BoxConstraints.expand(
                    height: MediaQuery
                        .of(context)
                        .size
                        .width,
                  ),
                  child: new Container(
                    decoration: new BoxDecoration(
                      color: new Color(0x88000000),
                    ),
                    child: new ButtonBar(
                      alignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new IconButton(icon: new Icon(Icons.message),
                          onPressed: cadet.phone != null ? () async {
                            await launch('sms:' + cadet.phone);
                          } : null,
                          color: Colors.white,
                          disabledColor: new Color(0xDDFFFFFF),),
                        new IconButton(icon: new Icon(Icons.phone),
                          onPressed: cadet.phone != null ? () async {
                            await launch('tel:' + cadet.phone);
                          } : null,
                          color: Colors.white,
                          disabledColor: new Color(0xDDFFFFFF),),
                        new IconButton(icon: new Icon(Icons.mail),
                          onPressed: cadet.un != null ? () async {
                            await launch('mailto:' + cadet.un + '@rit.edu');
                          } : null,
                          color: Colors.white,
                          disabledColor: new Color(0xDDFFFFFF),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              new Card(
                child: new Column(
                  children: <Widget>[
                    new GestureDetector(
                      onLongPress: () {
                        Clipboard.setData(new ClipboardData(
                            text: cadet.phone != null ? cadet.phone : ' '));
                        key.currentState.showSnackBar(
                            new SnackBar(
                              content: new Text("Copied to Clipboard"),));
                      },
                      child: new ListTile(
                        title: new Text('Phone', style: titleStyle),
                        subtitle: new Text(cadet.phone != null ? cadet.phone : 'N/A',
                            style: subTitleStyle),
                      ),
                    ),
                    new GestureDetector(
                      onLongPress: () {
                        Clipboard.setData(new ClipboardData(
                            text: cadet.un != null ? cadet.un + '@rit.edu' : ' '));
                        key.currentState.showSnackBar(
                            new SnackBar(
                              content: new Text("Copied to Clipboard"),));
                      },
                      child: new ListTile(
                        title: new Text('E-Mail', style: titleStyle),
                        subtitle: new Text(
                            cadet.un != null ? cadet.un + '@rit.edu' : 'N/A',
                            style: subTitleStyle),
                      ),
                    ),
                    new Divider(),
                    new ListTile(
                      title: new Text('School', style: titleStyle),
                      subtitle: new Text(cadet.school != null ? cadet.school : 'N/A',
                          style: subTitleStyle),
                    ),
                    new ListTile(
                      title: new Text('Major', style: titleStyle),
                      subtitle: new Text(cadet.major != null ? cadet.major : 'N/A',
                          style: subTitleStyle),
                    ),
                    new ListTile(
                      title: new Text('Hometown', style: titleStyle),
                      subtitle: new Text(
                          cadet.hometown != null ? cadet.hometown : 'N/A',
                          style: subTitleStyle),
                    ),
                    new Divider(),
                    new ListTile(
                      title: new Text('Rank', style: titleStyle),
                      subtitle: new Text(cadet.rank != null ? cadet.rank.name : 'N/A',
                          style: subTitleStyle),
                    ),
                    new ListTile(
                      title: new Text('AS Class', style: titleStyle),
                      subtitle: new Text(cadet.aslevel != null ? cadet.aslevel.name : 'N/A',
                          style: subTitleStyle),
                    ),
                    new ListTile(
                      title: new Text('Position', style: titleStyle),
                      subtitle: new Text(
                          cadet.position != null ? cadet.position.name : 'N/A',
                          style: subTitleStyle),
                    ),
                  ],
                ),
              )
            ],
          ),
        );
      });
  }
}

class CadetSearchPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, Store<AppState>>(
      converter: (store) => store,
      builder: (context, store) {
        return new Scaffold(
          body: new MaterialSearch<Cadet>(
            placeholder: 'Search',
            //placeholder of the search bar text input

            getResults: (String criteria) async {
              print(criteria);
              return store.state.cadets.where((cadet) => cadet.getFullName().contains(criteria)).map((cadet) =>
              new MaterialSearchResult<Cadet>(
                value: cadet, //The value must be of type <String>
                text: cadet.getFullName(), //String that will be show in the list
                icon: Icons.person,
              )).toList();
            },
            onSelect: (dynamic selected) {
              print((selected as Cadet).getFullName());
              Navigator.pop(context, (selected as Cadet).id);
            },
          ),
        );
    });
  }
}






///
/// CadetAddPage
/// Provides a list of cadets with checkboxes to bulk add
///
class CadetAddPage extends StatefulWidget {
  CadetAddPage({Key key}) : super(key: key);

  @override
  _CadetAddPageState createState() => new _CadetAddPageState();
}

class _CadetAddPageState extends State<CadetAddPage> {
  List<int> selectedTiles = [];
  bool batch = false;

  @override
  void initState() {
    super.initState();
  }

  Widget _cadetWidgetList(List<Cadet> cadets) {
    List<Widget> tiles = [];
    cadets.forEach((cadet) {
      if(batch) {
        tiles.add(
            new CheckboxListTile(
              title: new Text(cadet.getFullName()),
              value: selectedTiles.contains(cadet.id),
              onChanged: (selected) {
                if (selectedTiles.contains(cadet.id)) {
                  selectedTiles.remove(cadet.id);
                } else {
                  selectedTiles.add(cadet.id);
                }
                setState(() {
                  selectedTiles;
                });
              },
            )
        );
      }else{
        tiles.add(
            new ListTile(
              leading: new ProfileAvatar(cadet.getInitials(), cadet.image_uri),
              title: new Text(cadet.getFullName()),
              onTap: () {
                Navigator.pop(context, <int>[cadet.id]);
              },
            )
        );
      }
    });
    return new ListView(
      children: tiles,
    );
  }

  List<Widget> appBarButtons() {
    TextStyle buttonStyle = new TextStyle(fontSize: 16.0, color: Colors.white);
    if(batch){
      List<Widget> t = [];
      if(selectedTiles.length != 0){
        t.add(new FlatButton(
            onPressed: () {
              Navigator.pop(context, selectedTiles);
            },
            child: new Text('Add', style: buttonStyle)
        ));
      }
      t.add(new IconButton(
        icon: new Icon(Icons.close),
        onPressed: () {
          setState(() {
            batch = false;
          });
        },
      ));
      return t;
    }else{
      return <Widget>[
        new FlatButton(
            onPressed: () {
              setState(() {
                batch = true;
              });
            },
            child: new Text('Batch', style: buttonStyle)
        )
      ];
    }
  }

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, Store<AppState>>(
        converter: (store) => store,
        builder: (context, store)
        {
          return new Scaffold(
            appBar: AppBar(
                title:
                const Text('Add Cadets'),
                actions:
                appBarButtons()
            ),
            body: _cadetWidgetList(store.state.cadets)
            ,
            floatingActionButton: new FloatingActionButton(
                onPressed: ()
                async
                {
                  if(batch) {
                    selectedTiles.add(
                        await Navigator.push(context, new MaterialPageRoute(
                            builder: (context) => new CadetSearchPage())));
                    setState(() {
                      selectedTiles;
                    });
                  }else{
                    Navigator.pop(context, <int>[await Navigator.push(context, new MaterialPageRoute(
                        builder: (context) => new CadetSearchPage()))]);
                  }
                }
                ,
                child: new Icon(Icons.search)
            ),
          );
        }
    );
  }
}



