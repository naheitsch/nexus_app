import 'package:flutter/material.dart';
import 'package:nexus_app/redux/redux.dart';
import 'Updates.dart';

class DashboardPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, Store<AppState>>(
      converter: (store) => store,
      builder: (context, store) {
        return new RefreshIndicator(
          onRefresh: () {
            final action = new PullAnnouncementsAction();
            store.dispatch(action);
            store.dispatch(PullUpdatesAction());
            return action.completer.future;
          },
          child: new ListView(
          physics: const AlwaysScrollableScrollPhysics(),
          children: <Widget>[
            new DashboardAnnouncements(),
            new DashboardUpdates(),
          ]),
        );
      }
    );
  }
}

class DashboardAnnouncements extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, Store<AppState>>(
      converter: (store) => store,
      builder: (context, store) {
        print(store.state.announcements);
        if(store.state.announcements == null){
          if(store.state.isLoading['announcements']){
            return new Card(
              child: new Padding(
                padding: new EdgeInsets.all(16.0),
                child: new Center(
                  child: new CircularProgressIndicator(),
                )
              )
            );
          }
          return new Center(
              child: new Text('No UOD', style: Theme.of(context).textTheme.body1,)//new TextStyle(fontSize: 12.0, color: Colors.grey))
          );
        }
        List<Widget> list = [];
        store.state.announcements.forEach((ann) {
          print(ann);
          if(ann.title != null){
            print(ann.title);
            if(ann.title.contains('UOD')){
              print('using');
              list.add(
                  new Column(
                    children: <Widget>[
                      new Text(ann.text.replaceAll('_', ' '), style: Theme.of(context).textTheme.body1),//style: new TextStyle()),
                      new Text(ann.title.replaceAll('_', ' ').replaceAll('UOD', ''), style: Theme.of(context).textTheme.body2),//style: new TextStyle(fontSize: 12.0, fontWeight: FontWeight.w400)),
                    ],
                  )
              );
            }
          }
        });
        return new Card(
        child: new Padding(
            padding: const EdgeInsets.all(16.0),
            child: new Wrap(
              spacing: 16.0,
              runSpacing: 8.0,
              alignment: WrapAlignment.center,
              children: list,
            ),
          )
        );
      }
    );
  }
}

class DashboardUpdates extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, Store<AppState>>(
      converter: (store) => store,
      builder: (context, store) {
        if (store.state.updates == null || store.state.cadets == null)
          return new Card(
            child: new Padding(
                padding: new EdgeInsets.all(16.0),
                child: new Center(child: new CircularProgressIndicator())
            ),
          );
        if (store.state.updates.length == 0)
          return new Card(
            child: new Padding(
              padding: new EdgeInsets.all(16.0),
              child: new Column(
                children:  <Widget>[
                  new Text('No Updates', style: new TextStyle(color: Colors.grey)),
                ],
              )
            ),
          );

        List<ListTile> updateTiles = new List();
        store.state.updates.getRange(0, store.state.updates.length <= 5 ? store.state.updates.length : 5).forEach((update) {
          updateTiles.add(new ListTile(
              title: new Text(update.update_title),
              subtitle: new Text(update.updated_at),
              onTap: () {
                Navigator.push(context, new MaterialPageRoute(
                    builder: (context) => new UpdatePage(update)));
              }
          ));
        });
        return new Card(
          child: new Padding(
            padding: new EdgeInsets.all(8.0),
            child: new Column(
              children: <Widget>[
                new Text("Updates", style: new TextStyle(fontSize: 16.0)),
                new Divider(),
                new Column(
                  children: updateTiles,
                ),
                new Divider(),
                new FlatButton(
                  child: new Text("View All Updates"),
                  onPressed: (store.state.updates == null || store.state.updates.length <= 5) ? null : () {
                    Navigator.push(context, new MaterialPageRoute(builder: (context) => new UpdatesPage()));
                  },
                )
              ],
            ),
          ),
        );
    });
  }
}


