import 'package:flutter_redux/flutter_redux.dart';
import 'package:nexus_app/redux/state.dart';
import 'package:nexus_app/services/AuthorizedClient.dart';
import 'package:flutter/material.dart';

class ProfileAvatar extends StatelessWidget {
  String initials;
  String route;

  ProfileAvatar(this.initials, this.route);

  @override
  Widget build(BuildContext context) {
    String access_token = StoreProvider.of<AppState>(context).state.accessToken;
    Map<String, String> constants = StoreProvider.of<AppState>(context).state.constants;
    ImageProvider prov;
    if(route != null) {
      if (route.contains('profile_placeholder')) {
        prov = AuthorizedClient.getImage(
            route, constants, access_token);
      } else {
        prov = AuthorizedClient.getImage(
            '/api' + route, constants, access_token);
      }
    }
    return prov != null ? new CircleAvatar(
      backgroundImage: prov,
    ) : new CircleAvatar(
        backgroundColor: new Color(0xFFEEEEEE),
        child: new Text(initials)
    );
  }
}

