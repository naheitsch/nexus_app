import 'package:flutter/material.dart';
import 'package:nexus_app/models/models.dart';
import 'package:html2md/html2md.dart' as html2d;
import 'package:nexus_app/redux/redux.dart';

class UpdatePage extends StatelessWidget {
  final Update update;

  UpdatePage(this.update);

  @override
  Widget build(BuildContext context) {
    String text = html2d.convert(update.update_text);
    return new Scaffold(
      appBar: AppBar(
        title: new Text(update.update_title),
      ),
      body: new Padding(
        padding: new EdgeInsets.all(8.0),
        child: new ListView(
          children: <Widget>[
            new Text(text)
          ],
        ),
      ),
    );
  }
}

class UpdatesPage extends StatelessWidget {

  Widget _buildBody(List<Update> updates, context, store) {
    Widget child;
    if (updates == null) {
      child = new Center(
        child: new CircularProgressIndicator(),
      );
    }else {
      List<Widget> children;
      if(updates.length == 0){
        children = [
          new Padding(
            padding: new EdgeInsets.all(16.0),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Text('No Updates',
                    style: new TextStyle(fontSize: 16.0, color: Colors.grey)),
                new Text('Pull to Refresh',
                    style: new TextStyle(fontSize: 12.0, color: Colors.grey)),
              ],
            ),
          )

        ];
      }else {
        children = new List();
        updates.forEach((update) {
          children.add(new ListTile(
              title: new Text(update.update_title),
              subtitle: new Text(update.updated_at),
              onTap: () {
                Navigator.push(context, new MaterialPageRoute(
                    builder: (context) => new UpdatePage(update)));
              }
          ));
        });
      }
      child = new ListView(
          children: children,
      );
    }
    return new RefreshIndicator(
      onRefresh: () {
        final action = new PullUpdatesAction();
        store.dispatch(action);
        return action.completer.future;
      },
      child: child,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, Store<AppState>>(
      converter: (store) => store,
      builder: (context, store) {
        return new Scaffold(
        appBar: AppBar(
        title: const Text('Updates')
        ),
        body: _buildBody(store.state.updates, context, store),
        );
      });
    }
}


