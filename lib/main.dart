import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:nexus_app/ui/presenters/Events.dart';
import 'package:redux/redux.dart';
import 'package:nexus_app/redux/reducers.dart';
import 'package:nexus_app/redux/actions.dart';
import 'package:nexus_app/redux/middleware.dart';
import 'package:nexus_app/redux/state.dart';
import 'colors.dart';
import 'package:nexus_app/ui/presenters/ProfileAvatar.dart';
import 'package:nexus_app/ui/presenters/LoginPage.dart';
import 'package:nexus_app/ui/presenters/Cadets.dart';
import 'package:nexus_app/services/AuthorizedClient.dart';

import 'package:nexus_app/ui/presenters/DashboardPage.dart';
import 'package:nexus_app/ui/presenters/Updates.dart';

final ThemeData _appTheme = _buildTheme();

void main() => runApp(new NexusApp());

ThemeData _buildTheme() {
  return new ThemeData(
      accentColor: secondary,
      primaryColor: primary,
      buttonColor: primary,
      scaffoldBackgroundColor: surface,
      cardColor: Colors.white,
      fontFamily: 'Oxygen',
      textTheme: new TextTheme(
        title: new TextStyle(fontFamily: 'Oxygen', fontWeight: FontWeight.normal),
        subhead: new TextStyle(fontFamily: 'IBM'),
        button: new TextStyle(fontWeight: FontWeight.bold),
        caption: new TextStyle(fontFamily: 'IBM', fontWeight: FontWeight.w100),
        body1: new TextStyle(fontFamily: 'IBM', fontWeight: FontWeight.w100),
        body2: new TextStyle(fontFamily: 'IBM', fontWeight: FontWeight.bold, fontSize: 11.0),
      )
  );
}
class NexusApp extends StatelessWidget {
  final store = new Store<AppState>(
    reducer,
    initialState: new AppState.loading(),
    middleware: [middleware],
  );

  NexusApp();

  @override
  Widget build(BuildContext context) {
    store.dispatch(CheckAuthenticationAction());
    return new StoreProvider(
      store: store,
      child: new MaterialApp(
        title: 'Nexus',
        theme: _appTheme,
        home: new App(),
      ),
    );
  }
}

class App extends StatelessWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  _pullData(Store<AppState> store) {
    store.dispatch(PullUserAction());
    store.dispatch(PullCadetsAction());
    store.dispatch(PullAnnouncementsAction());
    store.dispatch(PullUpdatesAction());
  }

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, Store<AppState>>(
      converter: (store) => store,
      builder: (context, store) {
        if(store.state.authenticated){
          print('== Authenticated ==');
          if(store.state.isLoading['user'] == false) {
            if (store.state.user == null) {
              _pullData(store);
            }
          }
        }
        return !store.state.authenticated ? new Scaffold(
          body: new Center(
            child: new FlatButton(onPressed: (){
              Navigator.push(
                  context,
                  MaterialPageRoute( builder: (context) => new LoginPage())
              ).then((_) {
                store.dispatch(CheckAuthenticationAction());
              });
            }, child: new Text("Sign In")),
        )) : new Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
              title: const Text('Dashboard')
          ),
          drawer: new Drawer(
            // Add a ListView to the drawer. This ensures the user can scroll
            // through the options in the Drawer if there isn't enough vertical
            // space to fit everything.
            child: new ListView(
              // Important: Remove any padding from the ListView.
              padding: EdgeInsets.zero,
              children: <Widget>[
                store.state.user != null ? new UserAccountsDrawerHeader(
                  accountName: new Text(store.state.user.getFullName()),
                  accountEmail: new Text(store.state.user.un),
                  currentAccountPicture: new ProfileAvatar(store.state.user.getInitials(), store.state.user.image_uri)) :
                new UserAccountsDrawerHeader(accountName: null, accountEmail: null),
                new Divider(color: Colors.white),
                new ListTile(
                  leading: new Icon(Icons.people),
                  title: new Text('Cadets', style: Theme
                      .of(context)
                      .textTheme
                      .button),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(context, new MaterialPageRoute(
                        builder: (context) => new CadetsPage()));
                  },
                ),
                new ListTile(
                  leading: new Icon(Icons.calendar_today),
                  title: new Text('Events', style: Theme
                      .of(context)
                      .textTheme
                      .button),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(context, new MaterialPageRoute(
                        builder: (context) => new EventsPage()));
                  },
                ),
                new ListTile(
                  leading: new Icon(Icons.description),
                  title: new Text('Updates', style: Theme
                      .of(context)
                      .textTheme
                      .button),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(context, new MaterialPageRoute(
                        builder: (context) => new UpdatesPage()));
                  },
                ),
                new ButtonBar(
                  alignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new IconButton(
                        icon: new Icon(Icons.exit_to_app),
                        onPressed: () {
                          Navigator.pop(context);
                          AuthorizedClient.deauthorize().then((_) {
                            store.dispatch(LogoutAction());
                            store.dispatch(CheckAuthenticationAction());
                          });
                        }
                    ),
                    //new IconButton(icon: new Icon(Icons.settings, color: Colors.white), onPressed: null),
                  ],
                )
              ],
            ),
          ),
          body: new DashboardPage(),
        );
      }
    );
  }
}

