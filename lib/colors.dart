import 'package:flutter/material.dart';

const blue = const Color(0xFF4680FF);
const yellow = const Color(0xFFffae44);
const red = const Color(0xFFFB617F);

const primary = const Color(0xFFfa7921);
const secondary = const Color(0xFF0c4767);
const surface = const Color(0xFFeaebed);
const warning = const Color(0xFFfe9920);
const danger = const Color(0xFFFB617F);
const dark = const Color(0xFF353535);