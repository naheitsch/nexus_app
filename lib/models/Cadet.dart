import 'Permission.dart';
import 'Position.dart';
import 'ASLevel.dart';
import 'Rank.dart';

class Cadet {
  int id;
  String un;
  ASLevel aslevel;
  int flight;
  Rank rank;
  Position position;
  String firstName;
  String lastName;
  String phone;
  String school;
  String major;
  String hometown;
  String image_uri;

  List<Permission> permissions;

  Cadet();

  String getFullName() {
    return this.firstName + " " + this.lastName;
  }

  String getInitials() {
    return this.firstName[0] + this.lastName[0];
  }

  Cadet.fromMap(Map map){
    id = map['o_id'];
    un = map['un'];
    aslevel = map['aslevel'] == null ? null : ASLevel.fromMap(map['aslevel']);
    if(map['flight'] != null){
      flight = map['flight']['o_id'];
    }
    rank = map['rank'] == null ? null : Rank.fromMap(map['rank']);
    position = map['primary_position'] == null ? null : Position.fromMap(map['primary_position']);
    firstName = map['firstName'];
    lastName = map['lastName'];
    phone = nullCheck(map['phone']);
    school = map['school'];
    major = map['major'];
    hometown = map['hometown'];
    if (map['image_uri'] != null) {
      if ((map['image_uri'] as String).contains('images/profile-pic.png'))
        image_uri = null;
      else
        image_uri = map['image_uri'];
    } else {
      image_uri = null;
    }
  }

  String nullCheck(dynamic w){
    return w == null ? null : w;
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'o_id': id,
      'rit_un': un,
      'aslevel': aslevel != null ? aslevel.id : null,
      'flight': flight,
      'rank': rank != null ? rank.id : null,
      'primary_position': position != null ? position.id : null,
      'firstName': firstName,
      'lastName': lastName,
      'phone': phone,
      'school': school,
      'major': major,
      'hometown': hometown,
      'image_uri': image_uri
    };
    return map;
  }
}

