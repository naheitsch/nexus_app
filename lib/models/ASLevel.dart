final String api_route = '/api/aslevel';
final String tableName = 'ASLevels';
class ASLevel{
  int id;
  String name;

  ASLevel();

  Map<String,dynamic> toMap(){
    Map<String,dynamic> map = {
      'o_id': id,
      'name': name
    };
    return map;
  }

  ASLevel.fromMap(Map map) {
    id = map['o_id'];
    name = map['name'];
  }
}

