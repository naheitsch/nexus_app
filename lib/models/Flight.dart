class Flight {
  int id;
  String name;

  Flight();

  Flight.fromMap(Map map){
    id = map['o_id'];
    name = map['flight_name'];
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'o_id': id,
      'flight_name': name
    };
    return map;
  }
}
