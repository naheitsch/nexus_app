import 'package:redux/redux.dart';
import 'state.dart';
import 'actions.dart';
import 'package:nexus_app/models/models.dart';
import 'package:nexus_app/services/AuthorizedClient.dart';

void middleware(Store<AppState> store, action, NextDispatcher next) {
  // If our Middleware encounters a `FetchTodoAction`
  if (action is PullUserAction) {
    AuthorizedClient.get(route: '/api/cadets/self').then((cad) async{
      AuthorizedClient.get(route: '/api/permissions/self').then((perm) async{
        print('== Pulled User and Permissions ==');
       Cadet temp = Cadet.fromMap(cad);
       List<Permission> perms = new List();
       print(perm);
       perm.forEach((val) {
         perms.add(Permission.fromMap(val));
       });
       temp.permissions = perms;
       store.dispatch(new PullUserSuccessAction(temp));
      }).catchError((Exception error) {
        store.dispatch(new APIErrorAction(error));
      });
    }).catchError((Exception error) {
      store.dispatch(new APIErrorAction(error));
    });
  }else if(action is PullAnnouncementsAction) {
    AuthorizedClient.get(route: '/api/announcements').then((ann) async{
      List<Announcement> temp = new List();
      ann.forEach((val) {
        temp.add(Announcement.fromMap(val));
      });
      print('== Pulled Announcements ==');
      store.dispatch(new PullAnnouncementsSuccessAction(temp));
      action.completer.complete();
    }).catchError((Exception error) {
      store.dispatch(new APIErrorAction(error));
    });
  }else if(action is PullUpdatesAction) {
    AuthorizedClient.get(route: '/api/updates?scope=published').then((cadets) async{
      List<Update> temp = new List();
      cadets.forEach((cadet) {
        temp.add(Update.fromMap(cadet));
      });
      print('== Pulled Updates ==');
      store.dispatch(new PullUpdatesSuccessAction(temp));
      action.completer.complete();
    }).catchError((Exception error) {
      store.dispatch(new APIErrorAction(error));
    });
  }else if(action is PullCadetsAction) {
    AuthorizedClient.get(route: '/api/cadets').then((cadets) async{
      List<Cadet> temp = new List();
      cadets.forEach((cadet) {
        temp.add(Cadet.fromMap(cadet));
      });
      print('== Pulled Cadets ==');
      store.dispatch(new PullCadetsSuccessAction(temp));
      action.completer.complete();
    }).catchError((Exception error) {
      store.dispatch(new APIErrorAction(error));
    });
  }else if(action is PullTermsAction) {
    AuthorizedClient.get(route: '/api/terms').then((terms) async{
      List<Term> temp = new List();
      terms.forEach((w) {
        temp.add(Term.fromMap(w));
      });
      print('== Pulled Terms ==');
      store.dispatch(new PullTermsSuccessAction(temp));
      action.completer.complete();
    }).catchError((Exception error) {
      store.dispatch(new APIErrorAction(error));
    });
  }else if(action is PullEventsAction) {
    AuthorizedClient.get(route: '/api/events').then((events) async{
      List<Event> etemp = new List();
      events.forEach((w) {
        etemp.add(Event.fromMap(w));
      });
      print('== Pulled Events ==');
      AuthorizedClient.get(route: '/api/eventtypes').then((types) async{
        List<EventType> temp = new List();
        types.forEach((w) {
          temp.add(EventType.fromMap(w));
        });
        print('== Pulled Event Types ==');
        store.dispatch(new PullEventTypesSuccessAction(temp));
        store.dispatch(new PullEventsSuccessAction(etemp));
        action.completer.complete();
      }).catchError((Exception error) {
        store.dispatch(new APIErrorAction(error));
      });
    }).catchError((Exception error) {
      store.dispatch(new APIErrorAction(error));
    });
  }else if(action is PullAttendanceAction) {
    print('Pulling attendance !!!!!!!');
    AuthorizedClient.get(route: '/api/attendance').then((att) async{
      List<Attendance> atemp = new List();
      att.forEach((w) {
        atemp.add(Attendance.fromMap(w));
      });
      print('== Pulled Attendance ==');
      AuthorizedClient.get(route: '/api/attendancetypes').then((types) async{
        List<AttendanceType> temp = new List();
        types.forEach((w) {
          temp.add(AttendanceType.fromMap(w));
        });
        print('== Pulled Attendance Types ==');
        store.dispatch(new PullAttendanceSuccessAction(atemp));
        store.dispatch(new PullAttendanceTypesSuccessAction(temp));
        action.completer.complete();
      }).catchError((Exception error) {
        store.dispatch(new APIErrorAction(error));
      });
    }).catchError((Exception error) {
      store.dispatch(new APIErrorAction(error));
    });
  }else if(action is CheckAuthenticationAction) {
    AuthorizedClient.retrieveAccessToken().then((token) {
      if(token != null) {
        AuthorizedClient.retrieveUsername().then((un) {
          AuthorizedClient.GetConstants(un).then((constants) {
            store.dispatch(new SaveConstantsAction(constants));
            store.dispatch(new SaveAccessTokenAction(token));
            store.dispatch(new IsAuthenticatedAction(true));
          });
        });
      }else{
        store.dispatch(new IsAuthenticatedAction(false));
      }
    });
  }

  // Make sure our actions continue on to the reducer.
  next(action);
}