import 'dart:async';

import 'package:nexus_app/models/models.dart';

// Authentication
class LogoutAction {}

class CheckAuthenticationAction {}
class IsAuthenticatedAction {
  final bool authenticated;
  IsAuthenticatedAction(this.authenticated);
}

class SaveAccessTokenAction{
  final String token;
  SaveAccessTokenAction(this.token);
}
class SaveConstantsAction{
  final Map<String, String> constants;
  SaveConstantsAction(this.constants);
}

// User
class PullUserAction {
}
class PullUserSuccessAction{
  final Cadet user;
  PullUserSuccessAction(this.user);
}

// Cadets
class PullCadetsAction {
  final Completer<Null> completer;
  PullCadetsAction({Completer<Null> completer}) : this.completer = completer ?? new Completer<Null>();
}
class PullCadetsSuccessAction {
  final List<Cadet> cadets;
  PullCadetsSuccessAction(this.cadets);
}

// Announcements
class PullAnnouncementsAction {
  final Completer<Null> completer;
  PullAnnouncementsAction({Completer<Null> completer}) : this.completer = completer ?? new Completer<Null>();
}
class PullAnnouncementsSuccessAction {
  final List<Announcement> announcements;
  PullAnnouncementsSuccessAction(this.announcements);
}

// Terms
class PullTermsAction {
  final Completer<Null> completer;
  PullTermsAction({Completer<Null> completer}) : this.completer = completer ?? new Completer<Null>();
}
class PullTermsSuccessAction {
  final List<Term> terms;
  PullTermsSuccessAction(this.terms);
}
class SelectTermsAction {
  final List<Term> terms;
  SelectTermsAction(this.terms);
}

// AsLevels
class PullASLevelsAction {
}
class PullASLevelsSuccessAction {
  final List<ASLevel> aslevels;
  PullASLevelsSuccessAction(this.aslevels);
}

// Attendance
class PullAttendanceAction {
  final Completer<Null> completer;
  PullAttendanceAction({Completer<Null> completer}) : this.completer = completer ?? new Completer<Null>();
}
class PullAttendanceSuccessAction {
  final List<Attendance> records;
  PullAttendanceSuccessAction(this.records);
}
class PullAttendanceTypesSuccessAction {
  final List<AttendanceType> types;
  PullAttendanceTypesSuccessAction(this.types);
}

// Events
class PullEventsAction {
  final Completer<Null> completer;
  PullEventsAction({Completer<Null> completer}) : this.completer = completer ?? new Completer<Null>();
}
class PullEventsSuccessAction {
  final List<Event> events;
  PullEventsSuccessAction(this.events);
}
class AddEventAction {
  final Event event;
  AddEventAction(this.event);
}
class PullEventTypesSuccessAction {
  final List<EventType> types;
  PullEventTypesSuccessAction(this.types);
}

// Ranks
class PullRanksAction {
}
class PullRanksSuccessAction {
  final List<Rank> ranks;
  PullRanksSuccessAction(this.ranks);
}

// Updates
class PullUpdatesAction {
  final Completer<Null> completer;
  PullUpdatesAction({Completer<Null> completer}) : this.completer = completer ?? new Completer<Null>();
}
class PullUpdatesSuccessAction {
  final List<Update> updates;
  PullUpdatesSuccessAction(this.updates);
}

// Positions
class PullPositionsAction  {
}
class PullPositionsSuccessAction{
  final List<Position> positions;
  PullPositionsSuccessAction(this.positions);
}

class APIErrorAction {
  final Exception error;
  APIErrorAction(this.error);
}

//Actions to mutate state
class AddAttendanceRecordAction {
  final Attendance record;

  AddAttendanceRecordAction(this.record);
}
