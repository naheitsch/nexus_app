import 'package:redux/redux.dart';
import 'package:nexus_app/models/models.dart';
import 'actions.dart';
import 'state.dart';

AppState reducer(AppState state, action) {
  if(action is LogoutAction){
    return new AppState();
  }else {
    return new AppState(
        user: userReducer(state.user, action),
        cadets: cadetsReducer(state.cadets, action),
        terms: termsReducer(state.terms, action),
        selectedTerms: selectedTermsReducer(state.selectedTerms, action),
        aslevels: aslevelsReducer(state.aslevels, action),
        attendance: attendanceReducer(state.attendance, action),
        attendanceTypes: attendanceTypesReducer(state.attendanceTypes, action),
        events: eventsReducer(state.events, action),
        eventTypes: eventTypesReducer(state.eventTypes, action),
        ranks: ranksReducer(state.ranks, action),
        updates: updatesReducer(state.updates, action),
        announcements: announcementsReducer(state.announcements, action),
        positions: positionsReducer(state.positions, action),
        accessToken: accessTokenReducer(state.accessToken, action),
        constants: constantsReducer(state.constants, action),
        authenticated: authenticationReducer(state.authenticated, action),
        isLoading: loadingReducer(state.isLoading, action)
    );
  }
}

// Loading
Map<String, bool> loadingReducer(Map<String, bool> isLoading, action){
  return {
    'user': userLoadingReducer(isLoading['user'], action),
    'cadets': cadetsLoadingReducer(isLoading['cadets'], action),
    'terms': false,
    'aslevels': false,
    'attendance': attendanceLoadingReducer(isLoading['attendance'], action),
    'events': eventsLoadingReducer(isLoading['events'], action),
    'ranks': false,
    'updates': updatesLoadingReducer(isLoading['updates'], action),
    'announcements': announcementsLoadingReducer(isLoading['announcements'], action),
    'positions': false,
  };
}
final userLoadingReducer = combineReducers<bool>([
  new TypedReducer<bool, PullUserAction>(_toggleLoading),
  new TypedReducer<bool, PullUserSuccessAction>(_toggleLoading),
]);
final cadetsLoadingReducer = combineReducers<bool>([
  new TypedReducer<bool, PullCadetsAction>(_toggleLoading),
  new TypedReducer<bool, PullCadetsSuccessAction>(_toggleLoading),
]);
final attendanceLoadingReducer = combineReducers<bool>([
  new TypedReducer<bool, PullAttendanceAction>(_toggleLoading),
  new TypedReducer<bool, PullAttendanceSuccessAction>(_toggleLoading),
]);
final eventsLoadingReducer = combineReducers<bool>([
  new TypedReducer<bool, PullEventsAction>(_toggleLoading),
  new TypedReducer<bool, PullEventsSuccessAction>(_toggleLoading),
]);
final updatesLoadingReducer = combineReducers<bool>([
  new TypedReducer<bool, PullUpdatesAction>(_toggleLoading),
  new TypedReducer<bool, PullUpdatesSuccessAction>(_toggleLoading),
]);
final announcementsLoadingReducer = combineReducers<bool>([
  new TypedReducer<bool, PullAnnouncementsAction>(_toggleLoading),
  new TypedReducer<bool, PullAnnouncementsSuccessAction>(_toggleLoading),
]);

bool _toggleLoading(bool current, action){
  return !current;
}

// Authentication
final authenticationReducer = combineReducers<bool>([
  new TypedReducer<bool, IsAuthenticatedAction>(_setAuthenticated),
]);
final accessTokenReducer = combineReducers<String>([
  new TypedReducer<String, SaveAccessTokenAction>(_setAccessToken),
]);
final constantsReducer = combineReducers<Map<String, String>>([
  new TypedReducer<Map<String, String>, SaveConstantsAction>(_setConstants),
]);

bool _setAuthenticated(bool authed, IsAuthenticatedAction action){
  return action.authenticated;
}
String _setAccessToken(String token, SaveAccessTokenAction action){
  return action.token;
}
Map<String, String> _setConstants(Map<String, String> constants, SaveConstantsAction action){
  return action.constants;
}

// User
final userReducer = combineReducers<Cadet>([
  new TypedReducer<Cadet, PullUserSuccessAction>(_setUser),
]);

Cadet _setUser(Cadet cadet, PullUserSuccessAction action){
  return action.user;
}

// Cadets
final cadetsReducer = combineReducers<List<Cadet>>([
  new TypedReducer<List<Cadet>, PullCadetsSuccessAction>(_setCadets),
]);

List<Cadet> _setCadets(List<Cadet> cadets, PullCadetsSuccessAction action){
  return action.cadets;
}

// Terms
final termsReducer = combineReducers<List<Term>>([
  new TypedReducer<List<Term>, PullTermsSuccessAction>(_setTerms),
]);
List<Term> _setTerms(List<Term> terms, PullTermsSuccessAction action){
  return action.terms;
}

final selectedTermsReducer = combineReducers<List<Term>>([
  //new TypedReducer<List<Term>, PullTermsSuccessAction>(_setSelectedTerms),
  //new TypedReducer<List<Term>, SelectTermsAction>(_setSelectedTerms),
]);

List<Term> _setSelectedTerms(List<Term> terms, action){
  if(action is PullTermsSuccessAction) {
    return action.terms.getRange(0, 1);
  }else if(action is SelectTermsAction) {
    return action.terms;
  }
}

// AsLevels
final aslevelsReducer = combineReducers<List<ASLevel>>([
  //new TypedReducer<List<ASLevel>, PullASLevelsAction>(null),
]);

// Attendance
final attendanceReducer = combineReducers<List<Attendance>>([
  new TypedReducer<List<Attendance>, PullAttendanceSuccessAction>(_setAttendance),
  new TypedReducer<List<Attendance>, AddAttendanceRecordAction>(_addAttendanceRecord),
]);
List<Attendance> _setAttendance(List<Attendance> records, PullAttendanceSuccessAction action){
  return action.records;
}
List<Attendance> _addAttendanceRecord(List<Attendance> records, AddAttendanceRecordAction action){
  records.removeWhere((att) => att.id == action.record.id);
  return records..add(action.record);
}
final attendanceTypesReducer = combineReducers<List<AttendanceType>>([
  new TypedReducer<List<AttendanceType>, PullAttendanceTypesSuccessAction>(_setAttendanceTypes),
]);
List<AttendanceType> _setAttendanceTypes(List<AttendanceType> types, PullAttendanceTypesSuccessAction action){
  return action.types;
}

// Events
final eventsReducer = combineReducers<List<Event>>([
  new TypedReducer<List<Event>, PullEventsSuccessAction>(_setEvents),
  new TypedReducer<List<Event>, AddEventAction>(_addEvent),
]);
List<Event> _setEvents(List<Event> events, PullEventsSuccessAction action){
  return action.events;
}
List<Event> _addEvent(List<Event> events, AddEventAction action){
  return events..add(action.event);
}
final eventTypesReducer = combineReducers<List<EventType>>([
  new TypedReducer<List<EventType>, PullEventTypesSuccessAction>(_setEventTypes),
]);
List<EventType> _setEventTypes(List<EventType> eventTypes, PullEventTypesSuccessAction action){
  return action.types;
}

// Ranks
final ranksReducer = combineReducers<List<Rank>>([
  //new TypedReducer<List<Rank>, PullRanksAction>(null),
]);

// Updates
final updatesReducer = combineReducers<List<Update>>([
  new TypedReducer<List<Update>, PullUpdatesSuccessAction>(_setUpdates),
]);

List<Update> _setUpdates(List<Update> prev, PullUpdatesSuccessAction action){
  return action.updates;
}

// Announcements
final announcementsReducer = combineReducers<List<Announcement>>([
  new TypedReducer<List<Announcement>, PullAnnouncementsSuccessAction>(_setAnnouncements),
]);

List<Announcement> _setAnnouncements(List<Announcement> prev, PullAnnouncementsSuccessAction action){
  return action.announcements;
}

// Positions
final positionsReducer = combineReducers<List<Position>>([
  //new TypedReducer<List<Position>, PullPositionsAction>(null),
]);

