import 'package:nexus_app/models/models.dart';

class AppState {
  final Cadet user;
  final List<Cadet> cadets;
  final List<Term> terms;
  final List<Term> selectedTerms;
  final List<ASLevel> aslevels;
  final List<Attendance> attendance;
  final List<AttendanceType> attendanceTypes;
  final List<Event> events;
  final List<EventType> eventTypes;
  final List<Rank> ranks;
  final List<Update> updates;
  final List<Announcement> announcements;
  final List<Position> positions;
  final String accessToken;
  final Map<String, String> constants;
  final bool authenticated;
  final Map<String, bool> isLoading;

  AppState(
      {
        this.user = null,
        this.cadets = null,
        this.terms = null,
        this.selectedTerms = null,
        this.aslevels = null,
        this.attendance = null,
        this.attendanceTypes = null,
        this.events = null,
        this.eventTypes = null,
        this.ranks = null,
        this.updates = null,
        this.announcements = null,
        this.positions = null,
        this.accessToken = null,
        this.constants = null,
        this.authenticated = false,
        this.isLoading = const {
          'user': false,
          'cadets': false,
          'terms': false,
          'aslevels': false,
          'attendance': false,
          'events': false,
          'ranks': false,
          'updates': false,
          'announcements': false,
          'positions': false,
        }
      });

  factory AppState.loading() => new AppState();

  AppState copyWith({
    Cadet user,
    List<Cadet> cadets,
    List<Term> terms,
    List<Term> selectedTerms,
    List<ASLevel> aslevels,
    List<Attendance> attendance,
    List<AttendanceType> attendanceTypes,
    List<Event> events,
    List<EventType> eventTypes,
    List<Rank> ranks,
    List<Update> updates,
    List<Announcement> announcements,
    List<Position> positions,
    String accessToken,
    Map<String, String> constants,
    bool authenticated,
    bool isLoading,
  }) {
    return new AppState(
      user: user ?? this.user,
      cadets: cadets ?? this.cadets,
      terms: terms ?? this.terms,
      selectedTerms: selectedTerms ?? this.selectedTerms,
      aslevels: aslevels ?? this.aslevels,
      attendance: attendance ?? this.attendance,
      attendanceTypes: attendanceTypes ?? this.attendanceTypes,
      events: events ?? this.events,
      eventTypes: eventTypes ?? this.eventTypes,
      ranks: ranks ?? this.ranks,
      updates: updates ?? this.updates,
      announcements: announcements ?? this.announcements,
      positions: positions ?? this.positions,
      accessToken: accessToken ?? this.accessToken,
      constants: constants ?? this.constants,
      authenticated: authenticated ?? this.authenticated,
      isLoading: isLoading ?? this.isLoading,

    );
  }

  @override
  int get hashCode =>
      user.hashCode ^
      cadets.hashCode ^
      terms.hashCode ^
      selectedTerms.hashCode ^
      aslevels.hashCode ^
      attendance.hashCode ^
      attendanceTypes.hashCode ^
      events.hashCode ^
      eventTypes.hashCode ^
      ranks.hashCode ^
      updates.hashCode ^
      announcements.hashCode ^
      positions.hashCode ^
      accessToken.hashCode ^
      constants.hashCode ^
      authenticated.hashCode ^
      isLoading.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is AppState &&
              runtimeType == other.runtimeType &&
              user == other.user &&
              cadets == other.cadets &&
              terms == other.terms &&
              selectedTerms == other.selectedTerms &&
              aslevels == other.aslevels &&
              attendance == other.attendance &&
              attendanceTypes == other.attendanceTypes &&
              events == other.events &&
              eventTypes == other.eventTypes &&
              ranks == other.ranks &&
              updates == other.updates &&
              announcements == other.announcements &&
              positions == other.positions &&
              accessToken == other.accessToken &&
              constants == other.constants &&
              authenticated == other.authenticated &&
              isLoading == other.isLoading;

  @override
  String toString() {
    return 'AppState{user: $user, isLoading: $isLoading, authenticated: $authenticated }';
  }
}
