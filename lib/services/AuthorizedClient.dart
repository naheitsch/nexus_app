import 'dart:io';
import 'dart:async';
import 'dart:convert';
import 'package:connectivity/connectivity.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter/material.dart';
import 'package:nexus_app/enum/TaskStatus.dart';
import 'package:http/http.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

class AuthorizedClient
{
  //Constants
  static String TOKEN_KEY = "AFSABRE_ACCESS_TOKEN";
  static String USERNAME_KEY = "AFSABRE_USERNAME";

  static StreamController authDoneController = new StreamController.broadcast();

  static Stream get authDone => authDoneController.stream;

  static Future<TaskStatus> deauthorize() async {
    await deleteAccessToken();
    await deleteUsername();
    return TaskStatus.SUCCESS;
  }

  static Future<TaskStatus> authenticate({String username, String password}) async {
    Client client = new Client();
    var cst = await GetConstants(username);
    debugPrint(' - Authentication Request - ');
    debugPrint('\tBase URI: ' + cst['BASE_URI']);

      return client.post(cst['BASE_URI'] + '/oauth/token',
        headers: {
          HttpHeaders.ACCEPT: 'application/json'
        },
        body: {
        'username': username,
        'password': password,
        'grant_type': cst["GRANT_TYPE"],
        'client_id': cst["CLIENT_ID"],
        'client_secret': cst["CLIENT_SECRET"],
        'scope': cst["SCOPE"]
      }).then((response) {
        debugPrint(response.body.toString());
        debugPrint('\tStatus Code: ' + response.statusCode.toString());
        if (response.statusCode == HttpStatus.OK) {
          Map parsedMap = json.decode(response.body);
          storeAccessToken(parsedMap['access_token']);
          storeUsername(username);
          authDoneController.add('AuthSuccess');
          return TaskStatus.SUCCESS;
        } else {
          return TaskStatus.FAILURE;
        }
      }).catchError((e) {
        print(e);
        return TaskStatus.FAILURE;
      }).timeout(new Duration(seconds: 6));
  }

   static ImageProvider getImage([String route, Map<String,String> constants, String access_token]) {
    Map<String, String> headers = {
      HttpHeaders.AUTHORIZATION: 'Bearer ' + access_token
    };
    try {
      NetworkImage net = new NetworkImage(
          constants['BASE_URI'] + route, headers: headers);
      return net;
    }catch(error) {
      print(error.toString());
      return null;
    }
  }

  // Should have a catchError block whenever this is called.
  static Future<dynamic> get({String route}) async {
    Client client = new Client();
    String token = await retrieveAccessToken();
    if(token == null)
      return null;
    var cst = await GetConstants();
    Map<String, String> headers = {
      HttpHeaders.ACCEPT: 'application/json',
      HttpHeaders.AUTHORIZATION: 'Bearer ' + token
    };
    return client.get(cst['BASE_URI'] + route, headers: headers).then((response){
      if(response.statusCode == HttpStatus.OK){
        return json.decode(response.body);
      }else{
        print(response.request);
        throw new HttpRequestError(response.statusCode);
      }
    }).timeout(new Duration(seconds: 6));
  }

  // Should have a catchError block whenever this is called.
  static Future<dynamic> post({String route, Map<String, dynamic> content }) async {
    Client client = new Client();
    String token = await retrieveAccessToken();
    print('got token');
    if(token == null)
      return null;
    print('tokennotnull');
    var cst = await GetConstants();
    print('gotconstants');
    Map<String, String> headers = {
      HttpHeaders.ACCEPT: 'application/json',
      HttpHeaders.AUTHORIZATION: 'Bearer ' + token
    };
    return client.post(cst['BASE_URI']+route, headers: headers, body: content).then((response) {
      print('response');
      print(response.body);
      if(response.statusCode < 300){
        return json.decode(response.body);
      }else{
        print(response.body);
        throw new HttpRequestError(response.statusCode);
      }
    }).timeout(new Duration(seconds: 6));
  }

  static Future<Map<String, String>> GetConstants([String username]) async {
    //"BASE_URI": "https://afnexus-dev.tranit.us",
    //"BASE_URI": "http://localhost",
    //"BASE_URI": "http://10.0.2.2",
    Map<String, String> DevConstants = {
      "BASE_URI": "http://192.168.1.24",
      "CLIENT_ID": "2",
      "CLIENT_SECRET": "Bunys4czEOFn0Us9o34gBXnQUl5kw9hDEaC0xNqB",
      "GRANT_TYPE": "password",
      "SCOPE": ""
    };
    Map<String, String> ProdConstants = {
      "BASE_URI": "https://afsabre.rit.edu",
      "CLIENT_ID": "7",
      "CLIENT_SECRET": "D36Kqv1UwKcJlchC8brQkIeUZeDAU6pgS3ren2bu",
      "GRANT_TYPE": "password",
      "SCOPE": ""
    };
    if(username == null){
      username = await retrieveUsername();
    }
    if(username.contains("dev", 2))
    {
      return DevConstants;
    }
    return ProdConstants;
  }

  static Future<bool> checkConnection() async {
    return null != await (new Connectivity().checkConnectivity());
  }

  // KeyChain Functions
  static FlutterSecureStorage _getSecureStorage(){return new FlutterSecureStorage();}

  static Future<String> retrieveUsername() async {
    return await _getSecureStorage().read(key: USERNAME_KEY);
  }
  static TaskStatus storeUsername(String username){
    debugPrint('\tStoring Username: ' + username);
    try {
      _getSecureStorage().write(key: USERNAME_KEY, value: username);
    }catch(e){
      print(e);
      return TaskStatus.FAILURE;
    }
    return TaskStatus.SUCCESS;
  }
  static Future<TaskStatus> deleteUsername() async {
    try {
     await _getSecureStorage().delete(key: USERNAME_KEY);
    }catch(e){
      print(e);
      return TaskStatus.FAILURE;
    }
    return TaskStatus.SUCCESS;
  }
  static Future<String> retrieveAccessToken() async {
    return await _getSecureStorage().read(key: TOKEN_KEY);
  }
  static TaskStatus storeAccessToken(String accessToken){
    debugPrint('\tStoring Access Token: ' + accessToken);
    try {
      _getSecureStorage().write(key: TOKEN_KEY, value: accessToken);
    }catch(e){
      print(e);
      return TaskStatus.FAILURE;
    }
    return TaskStatus.SUCCESS;
  }
  static Future<TaskStatus> deleteAccessToken() async {
    try {
      await _getSecureStorage().delete(key: TOKEN_KEY);
    }catch(e){
      print(e);
      return TaskStatus.FAILURE;
    }
    return TaskStatus.SUCCESS;
  }
}

class HttpRequestError{
  int status;
  HttpRequestError(this.status);
  @override
  String toString() {
    return "HTTP Status: " + status.toString();
  }
}

