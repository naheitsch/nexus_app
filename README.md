# Nexus App

Companion app for the 
[Nexus Portal](https://bitbucket.org/christophert/afnexus/src/staging/)

Built with Google's Flutter which utilizes Dart (much alike JS)

Follow the links below to get started with installing the Flutter SDK and Android Studio (the recommended IDE for Dart and Flutter projects.

## Release Roadmap

### v1.0.0 (Current)

* Contact list of all Nexus users
* UOD and recent updates
* Event and Attendance Lists
* Barcode scanning and attendance addition for admin users
* Updates List

### v1.2.0

* New more fluid UI
* Properly formatted Updates
* Update Editing/Creation
* Update Approval/Publishing
* Cadet Bios
* Profile Editing


## Getting Started

For help getting started with Flutter, view our online
[documentation](https://flutter.io/).

### iOS

Run `pod update` inside the ios folder.

Run `flutter run` to launch emulator.
